/// <reference types='cypress' />

describe('Sessions', () => {
  beforeEach(() => {
    cy.visit('/conference');
    cy.get('h1').contains('View Sessions').click();
    cy.url().should('include', '/sessions');
    cy.get('a').contains('Submit a Session!').click();
  });

  it('should navigate to submit sessions page', () => {
    // cy.visit('/conference');
    // cy.get('h1').contains('View Sessions').click();
    // cy.url().should('include', '/sessions');
    // cy.get('a').contains('Submit a Session!').click();
    cy.url().should('include', '/sessions/new');
  });

  it('should submit a session successfully', () => {
    // Filling the form with session information
    cy.contains('Title').type('New Session');
    cy.contains('Description').type('Cool session typed from Cypress');
    cy.contains('Day').type('Thursday');
    cy.contains('Level').type('Beginner');

    // Submit the form
    cy.get('form').submit();

    // Validate that form was submitted successfully
    cy.contains('Session Submitted Successfully');
  });
});
