/// <reference types='cypress' />

describe('Navigation', () => {
  it('should navigate to conference page', () => {
    cy.clickViewSessions();
    cy.url().should('include', '/sessions');
  });
});
