/// <reference types='cypress' />

const thursdaySessionsData = {
  data: {
    intro: [
      {
        id: '82048',
        title: "10 Things You'll Hate About React",
        startsAt: '8:45',
        day: 'Thursday',
        room: 'Sol',
        level: 'Introductory and overview',
        speakers: [
          {
            id: 'a25e6534-f680-4487-83da-9f58329af3a9',
            name: 'Willie Wells',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
    ],
    intermediate: [
      {
        id: '85256',
        title: 'React Native and Flutter, Whats What',
        startsAt: '8:45',
        day: 'Thursday',
        room: 'Saturn',
        level: 'Intermediate',
        speakers: [
          {
            id: '8de6a283-f104-432b-ad08-f775a5fae6a4',
            name: 'Sally Watts',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
      {
        id: '81852',
        title: 'Change Your Culture with CI/CD',
        startsAt: '8:45',
        day: 'Thursday',
        room: 'Neptune',
        level: 'Intermediate',
        speakers: [
          {
            id: '01b15b1c-44e4-4121-b057-ae7eb9c83934',
            name: 'Rafael Moore',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
    ],
    advanced: [
      {
        id: '100073',
        title: 'Modern Testing Principles',
        startsAt: '10:00',
        day: 'Thursday',
        room: 'Earth',
        level: 'Advanced',
        speakers: [
          {
            id: 'd87a8517-8cc1-4867-8461-e496c143ded2',
            name: 'Dakotah Cox',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
      {
        id: '77253',
        title: "I'm Going To Make You Start Hating CSS.",
        startsAt: '11:15',
        day: 'Thursday',
        room: 'Jupiter',
        level: 'Advanced',
        speakers: [
          {
            id: 'f4f1eb31-8574-4e8a-a94a-ee0aef8c28d5',
            name: 'Julian Richardson',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
      {
        id: '82606',
        title: 'When Azure AD is Not Enough',
        startsAt: '11:15',
        day: 'Thursday',
        room: 'Earth',
        level: 'Advanced',
        speakers: [
          {
            id: 'e9ef29b1-ab04-4328-ba58-38b1c047e0bf',
            name: 'Ray Gardner',
            __typename: 'Speaker',
          },
        ],
        __typename: 'Session',
      },
    ],
  },
};

describe('Sessions page', () => {
  beforeEach(() => {
    cy.clickViewSessions();
    cy.url().should('include', '/sessions');

    // Define aliases here
    cy.get('[data-cy=AllSessions]').as('AllSessionsBtn');
    cy.get('[data-cy=Wednesday]').as('WednesdayBtn');
    cy.get('[data-cy=Thursday]').as('ThursdayBtn');
    cy.get('[data-cy=Friday]').as('FridayBtn');
  });

  it('should navigate to conference sessions page and view day filter buttons', () => {
    // Validate that buttons to filter by day exists
    cy.get('@AllSessionsBtn');
    cy.get('@WednesdayBtn');
    cy.get('@ThursdayBtn');
    cy.get('@FridayBtn');
  });

  it('should filter sessions and only display Wednesday sessions when Wednesday buttion is clicked', () => {
    cy.intercept('POST', 'http://localhost:4000/graphql').as('getSessionInfo');
    cy.get('@WednesdayBtn').click();
    // cy.wait('@getSessionInfo');

    // Assertions
    // Assert there are 100 sessions after Wednesday button is clicked
    cy.dataCy('day').should('have.length', 21); // dataCy is a custom command

    cy.get('[data-cy=day]').contains('Wednesday').should('be.visible');
    cy.get('[data-cy=day]').contains('Thursday').should('not.exist');
    cy.get('[data-cy=day]').contains('Friday').should('not.exist');
  });

  it('should filter sessions and only display Thursday sessions when Thursday buttion is clicked', () => {
    // Stubbing a response data
    cy.intercept(
      'POST',
      'http://localhost:4000/graphql',
      thursdaySessionsData
    ).as('getSessionInfo');
    cy.get('@ThursdayBtn').click();
    // Wait for the Server to respond back to aliased route
    cy.wait('@getSessionInfo');

    // Assertions
    cy.get('[data-cy=day]').should('have.length', 6);
    cy.get('[data-cy=day]').contains('Thursday').should('be.visible');
    cy.get('[data-cy=day]').contains('Wednesday').should('not.exist');
    cy.get('[data-cy=day]').contains('Friday').should('not.exist');
  });

  it('should filter sessions and only display Friday sessions when Friday buttion is clicked', () => {
    // Stubbing a response data
    cy.intercept('POST', 'http://localhost:4000/graphql', {
      fixture: 'sessions.json',
    }).as('getSessionInfo');
    cy.get('@FridayBtn').click();

    // Assertions
    cy.get('[data-cy=day]').should('have.length', 5);
    cy.get('[data-cy=day]').contains('Friday').should('be.visible');
    cy.get('[data-cy=day]').contains('Thursday').should('not.exist');
    cy.get('[data-cy=day]').contains('Wednesday').should('not.exist');
  });

  it('should filter sessions and only display AllSessions sessions when AllSessions buttion is clicked', () => {
    cy.intercept('POST', 'http://localhost:4000/graphql').as('getSessionInfo');
    cy.get('[data-cy=AllSessions]').click();
    cy.wait('@getSessionInfo');

    // Assertions
    cy.get('[data-cy=day]').contains('Friday').should('be.visible');
    cy.get('[data-cy=day]').contains('Thursday').should('be.visible');
    cy.get('[data-cy=day]').contains('Wednesday').should('be.visible');
  });
});
