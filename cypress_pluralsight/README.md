### Course

* https://app.pluralsight.com/course-player?clipId=4ecb87f7-a628-4f71-a2bf-01c08de0b01a

### Unit test

* Tests an isolated unit in your code(a single function, class)

### Integration test

* Testing code with Dependencies
* Tests more than just an isolated unit
* API, Component tests

### End to End Test

* Tests full flow of application
* Interacts with the application like an end user

### Cypress

* Framework agnostic, can be used React and Angular
* End to End Testing Framework for anything that runs in the Browser
* It is Fast, Easy and Cost Effective
* Has native access to everything since it operates within our Application
* Impossible for Cypress to miss elements

### Cypress Features

* Time travel & debugging
  * Hover over Cypress Command and see what happened
* Automatic Waiting
* Reliable and Fast
* Screenshots and Videos
* Spies, Stubs and Clocks
* Network Traffic Control

### Simple Tests using Cypress

* **describe** to group bunch of similar tests
* **it** name of the test
* describe and it comes from Mocha(Testing Framework)
* All Cypress commands begin wity **cy**
  * cy.visit("/conference")
  * cy.get("h1").contains("View Sessions")
* Before Cypress
  * Framework(Mocha, Jasmine, Karma)
  * Assertion Library(Chai, Expect.js)
  * Install Selenium
  * Selenium Wrapper(Protractor, Nightwatch)
  * Other(Sinon, TestDouble)
* Cypress Contains all of them in one testing framework

### Tradeoffs

* Write Cypress tests only for applications that you can control(Need source code of App and run it)
* Cannot visit 2 domains of different origins in same test, but can be done in different tests
* Iframe support is limited
* Workarounds for cy.hover() and cy.tab()

# Cypress-Fundamentals(Author)

Code repo for Pluralsight course Cypress 9 Fundamentals by Adhithi Ravichandran

Course on Pluralsight: https://app.pluralsight.com/library/courses/cypress-9-fundamentals/

Branch details:

master - The code to start with while starting the course (before)

M3-Setup-Module - Code for Module 3 - Set up, write and run first tests

M4-Cypress-Concepts - Code for Module 4 - Cypress Core Concepts

M5-Network-Requests - Code for Module 5 - Testing Network Requests

M6-Ecosystem - Code for Module 6 - Exploring Cypress Ecosystem

### Installation and Environment Settings

* VS code Extension: Cypress Snippets
* cd to app folder(Frontend) and npm i and npm start
* cd to api and npm i and npm start

### Cypress Setup

* npm i cypress --save-dev
* Run npm i in the root folder as cypress is already added as dev-dependency in the package.json
* npx cypress open
  * Command for launching the Cypress Test runner
  * This also creates a cypress folder with existing tests
* Clicking on the spec file runs the test in localhost:1337
  * Make sure to have app and api running to be able to run cypress tests on localhost:1337

### Write and Run First Test

* To enable Intellisense, add this(/// <reference types="cypress">) in the head of the test file
  * This will turn on Intellisense on a per file basis
* describe and it from Mocha testing framework
* Cypress commands starting with cy
  * cy.visit
  * cy.get
* Assertion with should

### Organizing Tests

* 5 sub-folders in cypress folder
* Folder structure
  * fixtures folder for the Fixtures
    * Access using cy.fixture() command to load the data
    * Used for stubbing Network Request
  * integration folder for the test files
  * plugins to modify or extend internal behavior of Cypress
    * It executes a node before the project is loaded, before the browser launches, and during your test execution
    * While the Cypress test execute in the Browser, the plugins file runs in the background node process
    * It gives your test the ability to access the file system and the rest of the operating system by calling the cypress.task command
  * support for placing reusable code like custom commands, utilities etc
* Hooks from Mocha
  * before
    * root-level hook
    * runs once before all tests
  * beforeEach
    * root-level hook
    * runs before every test block
  * afterEach
    * runs after each test block
  * after
    * runs once all tests are done

### Interacting with Elements Using Commands

* Cypress comes with an in-built set of commands to interact with Web Page
* They are categorized as parent, child and dual commands
* You can also create custom commands, and override existing commands
* Parent commands begins a new chain of Cypress commands
  * cy.visit('/conference')
  * This cannot be chained off another Cypress comand
  * Other examples are get, request(For making Network requests), exec(To run system command), intercept(Manage the behavior of Network requests and responses)
    * (Note: All begin with cy.)
* Child commands can be Chained off a Parent command, or another Child command
  * cy.get('[data-cy=speakerProfile]').click()
    * Child command here is click
  * cy.get('[data-cy=sessionTitle]').type('My new session')
  * Other examples are find, should, scrollIntoView, submit
* Dual commands can either Start a Chain or be Chained off an existing one
  * cy.contains(), screenshot(), scrollTo(), wait()

### Commands Demo

* Add baseUrl in cypress.json file so that you don't have to mention the whole url again and again
* Add common steps for each test in beforeEach hook

### Selectors

* Cypress will automatically calculate a unique selector to use targeted element
  * data-cy, data-test, data-testid
  * id, class, tag, atributes, nth-child
* You can control how a selector is determined using Cypress.SelectorPlayground API
* Don't use Generic, coupled to styling, id, attribute, actual text
* Best option: Use data-cy as it is isolated from all changes
* Use data-* attributes to provide context to your selectors and **isolate them from CSS or JS changes**

### Selectors Demo

* IN Sessions.jsx add data-cy attribute to buttons

### Assertions

* https://docs.cypress.io/guides/references/assertions
* Common Cypress Assertions below
```js
cy.contains("[data-cy=day]", "Wednesday").should("be.visible");
cy.url().should("include", "/sessions");
cy.get("[data-cy=sessionList]").should("have.length", 250);
cy.get("[data-cy=profile]").should("not.exist");

```

### Assertions Demo

* Check sessions.spec.js 2nd test Wednesday

### Cypress Retry-ability

* If assertion following a DOM query command fails - Keeps retrying until timeout
  * .get(), .find(), .contains()
* Commands that may change the state of application are not retried
  * .click()
* Only last command before assertion is retried
* Default timeout 4000ms

### Aliases

* Aliases using **as** and access using **@**

### Testing Network Requests

* 2 ways
  * Stub responses
  * Use Server responses
* Advantages of Stub Responses
  * Can control every aspect of response(body status ,headers, delay)
  * Extermely fast and reliable responses with no flakiness in tests
  * Perfect for JSON responses
* Advantages of Using Server Responses
  * True end-to-end tests that wait for a response from the server
  * Great for traditional server-side HTML rendering
  * But Tests will be slower and unreliable
* Best Practice
  * Use stubbed response tests more often
  * Don't use stubbed responses for server-side rendering architecture
  * Avoid stubs for critical paths like login

### Intercept Command

* Spying and Stubbing backend with cy.intercept()
* Declaratively wait for requests and responses using cy.wait() command
* Waiting on aliased route
  * Makes tests more robust with less flake
* Spying request
```js
cy.intercept('/users/**'); 
// Above Default is GET which is same as cy.intercept('GET', '/users/**')
```
* It can passively listen for matching routes and apply aliases, to them without mainpulating the request or its response in any way
```js
cy.intercept({method: 'GET', url: '/users/**', hostname: 'localhost'})
```
* intercept command doesn't change the request or the response, it just spies
* wait command when given an alias checks if request went there
* Use **it.only** to run only those tests
* Mute a test by using **xit**

### Stubbing with cy.intercept()

```js
cy.intercept({method: 'GET', url: '/users/*'}, []).as('getUsers');
```
* Here 2nd argument of [] is the stubbed response
* Stub headers, status code and body
```js
cy.intercept('/not-found', {
  statusCode: 404,
  body: "404 not found!",
  headers: {
    "x-not-found": "true"
  }
})
```
* Stub using fixtures
```js
cy.intercept('GET', '/activities/*', { fixture: 'activities.json' })
```
* Once a route is matched, we use cy.wait command to wait on that aliased route
* Stubbing response data in Thursday test in sessions.spec.js

### Stubbing from Fixtures

* Create fixtures in cypress/fixtures folder
* Create sessions.json containing Friday's data
* To include the fixuture, use fixture key in the 3rd argument object and use the file name

### Request Command

* cy.request() command makes an HTTP request. Request reaches your server
* It requires that the server sends a response. It can time out waiting for the server to respond
* It only runs assertions you have chained once and will not retry
```js
cy.request('https://jsonplaceholder.cypress.io/comments').as('comments');

cy.get('@comments').should((response) => {
  expect(response.body).to.have.length(500);
  expect(response).to.have.property('headers');
  expect(response).to.have.property('duration');
})
```
* Not used widely because of intercept command

### Screenshots and Videos

* npx cypress run
  * Run Cypress tests in headless mode
* When it runs in headless mode, it automatically creates videos and is added in the videos folder
* https://docs.cypress.io/guides/references/configuration#Videos
* All this settings in cypress.json
* When there is test failure, Cypress automatically creates screenshots in screenshots folder
* cy.screenshot() command can be inserted within code
  * Takes a screenshot at that point in time

### Custom Commands

* Check support/commands.js and sessions.spec.js for usage
  * Check dataCy and clickViewSessions command added
* Commands which you can use across several spec files

### Cross-browser Support

* Chrome, Firefox and other popular browsers
* cypress run --browser firefox
* By default on Electron

### Cypress Plugins

* Usecase
  * Modify or extend internal behavior of Cypress
  * Write your own custom code that executes during Cypress stages
  * Tap into the node process running outside the browser
  * Alter configuration and environment variables
  * Customize how test code is transpiled and sent to browser
  * Manipulate a database
* Placed in cypress/plugins/index.js
* 2 Parameters
  * on - hook into various events that cypress emits
  * config = resolved Cypress config
* List of Cypress Plugins in https://docs.cypress.io/plugins/
  * Percy for Visual Testing

### TypeScript Support

* cypress/tsconfig.json
```json
{
  "compilerOptions": {
    "target": "es5",
    "lib": ["es5", "dom"],
    "types": ["cypress"]
  },
  "include": ["**/*.ts"]
}
```
* Changes to support/index.ts for Types for Custom Commands
```ts
declare global {
  namespace Cypress {
    interface Chainable {
      dataCy(value: string): Chainable<Element>;
    }
  }
}
```

### Cypress Docs

* https://docs.cypress.io/api/table-of-contents