### 4. Getting Started with Reducers

* npm i @ngrx/store
* ng add @ngrx/store(This is even better than the 1st one)
* Store and Reducers are tightly coupled together
* Check shoppingListReducer in shopping-list folder
* A Reducer is just a function receiving state and action as arguments
* initialState most of the times will be an object

### 5. Adding Logic to the Reducer

* Copy the old state using spread syntax and override the property